@can('accept', $model)
<a title="Mark this as Best Answer (Click again to undo)" class="{{ $model->status }} mt-2"
    onclick="event.preventDefault(); document.getElementById('accept-answer-{{ $model->id }}').submit();">
    <i class="fa fa-check fa-2x"></i>
</a>
<form action=" {{ route('answers.accept', $model->id) }} " method="post" id="accept-answer-{{ $model->id }}"
    style="display: none;">
    @csrf
</form>
@else
@if ($model->is_best)
<a title="This is the best answer" class="{{ $model->status }} mt-2">
    <i class="fa fa-check fa-2x"></i>
</a>
@endif
@endcan
